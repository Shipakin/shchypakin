﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zenject.SpaceFighter
{
    public class GameOverPanelHandler : IInitializable, IDisposable
    {
        readonly SignalBus _signalBus;

        readonly Player _player;

        public GameOverPanelHandler(

            SignalBus signalBus,
            Player player)
        {

            _signalBus = signalBus;
            _player = player;
        }

        public void Initialize()
        {
            _signalBus.Subscribe<PlayerDiedSignal>(OnPlayerDied);
            _signalBus.Subscribe<Continue>(OnContinue);
            _signalBus.Subscribe<Retry>(OnRetry);
        }

        public void Dispose()
        {
            _signalBus.Unsubscribe<PlayerDiedSignal>(OnPlayerDied);
        }



        void OnPlayerDied()
        {
            Time.timeScale = 0f;
        }

        void OnContinue()
        {
            Time.timeScale = 1f;
            _player.RestoreHealth();
        }

        void OnRetry()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}

