using UnityEngine;

#pragma warning disable 649

namespace Zenject.SpaceFighter
{
    public class ControlsDisplay : MonoBehaviour
    {
        [SerializeField]
        float _leftPadding;

        [SerializeField]
        float _topPadding;

        [SerializeField]
        float _width;

        [SerializeField]
        float _height;

        [Inject]
        public void Construct(SignalBus signalBus)
        {
            signalBus.Subscribe<PlayerDiedSignal>(OnPlayerDied);
            signalBus.Subscribe<Continue>(OnContinue);
        }

        public void OnGUI()
        {
            var bounds = new Rect(_leftPadding, _topPadding, _width, _height);
            GUI.Label(bounds, "CONTROLS:  WASD to move, Mouse to aim, Left Mouse to fire");
        }

        public void OnPlayerDied()
        {
            this.enabled = false;
        }

        public void OnContinue()
        {
            this.enabled = true;
        }
    }
}

