﻿using ModestTree;
using UnityEngine;


namespace Zenject.SpaceFighter
{
    public class GameOverGui : MonoBehaviour
    {
        [SerializeField]
        GameObject _gameOverMenuPrefab;
        SignalBus _signalBus;


        [Inject]
        public void Construct(SignalBus signalBus)
        {

            _signalBus = signalBus;
            _signalBus.Subscribe<PlayerDiedSignal>(Show);
        }

        public void Show()
        {

            _gameOverMenuPrefab.SetActive(true);
        }

        public void Continue()
        {
            _signalBus.Fire<Continue>();
            Hide();
        }

        public void Retry()
        {
            _signalBus.Fire<Retry>();
            Hide();
        }

        public void Hide()
        {
            _gameOverMenuPrefab.SetActive(false);
        }
    }
}


